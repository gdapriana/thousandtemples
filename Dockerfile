FROM node:18-alpine
LABEL authors="gedeapriana"
WORKDIR /app
COPY package* .
RUN npm i
COPY . .
CMD ["npm", "run", "dev"]

