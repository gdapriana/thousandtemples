import type { Metadata } from "next";
import "./globals.css";
import { NextAuthProvider } from "@/lib/providers/nextauth";
import { LoadingProvider } from "@/lib/providers/loading";
import { Inter } from "next/font/google";
import { ScrollProvider } from "@/lib/providers/scroll";

export const metadata: Metadata = {
  title: "thousandtemples",
  description: "Generated by create next app",
};

const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <NextAuthProvider>
          <LoadingProvider>
            <ScrollProvider>{children}</ScrollProvider>
          </LoadingProvider>
        </NextAuthProvider>
      </body>
    </html>
  );
}
