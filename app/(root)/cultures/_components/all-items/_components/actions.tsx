import { Button } from "@/components/ui/button";
import { Dispatch, SetStateAction } from "react";

export const Actions = ({
  takeItem,
  favorited,
  commented,
  url
}: {
  takeItem: number,
  favorited: {activeFavorited: boolean; setActiveFavorited: Dispatch<SetStateAction<boolean>>}
  commented: {activeCommented: boolean; setActiveCommented: Dispatch<SetStateAction<boolean>>}
  url: {activeUrl: string; setActiveUrl: Dispatch<SetStateAction<string>>}
}) => {
  return (
    <main className="py-4 border-y flex flex-wrap justify-end items-center gap-4">
      <div className="flex justify-center items-center gap-2">
        <Button variant={favorited.activeFavorited ? "default" : "secondary"} onClick={() => {
          favorited.setActiveFavorited(!favorited.activeFavorited)
          commented.setActiveCommented(!commented.activeCommented)
          url.setActiveUrl(`${process.env.NEXT_PUBLIC_SERVER}/api/cultures?saved=1&take=${takeItem}`)
        }}>Most Favorited</Button>
        <Button variant={commented.activeCommented ? "default" : "secondary"} onClick={() => {
          commented.setActiveCommented(!commented.activeCommented)
          favorited.setActiveFavorited(!favorited.activeFavorited)
          url.setActiveUrl(`${process.env.NEXT_PUBLIC_SERVER}/api/cultures?commented=1&take=${takeItem}`)
        }}>Most Commented</Button>
      </div>
    </main>
  )
}