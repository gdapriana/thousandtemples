import { CultureProps } from "@/lib/utils"
import { Card } from "@/app/(root)/cultures/_components/all-items/_components/card";

export const Items = ({ cultures }: { cultures: CultureProps[] | undefined }) => {
  return (
    <main className="grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
      {cultures?.map((culture: CultureProps) => {
        return <Card culture={culture} key={culture.id} />
      })}
    </main>
  )
}