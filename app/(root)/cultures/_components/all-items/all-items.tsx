'use client'
import { Button } from "@/components/ui/button"
import { useContext, useEffect, useState } from "react"
import { Actions } from "@/app/(root)/cultures/_components/all-items/_components/actions"
import { CultureProps } from "@/lib/utils"
import { LoadingContext } from "@/lib/providers/loading"
import { Items } from "@/app/(root)/cultures/_components/all-items/_components/items"

export const AllItems = () => {

  const {setLoading } = useContext(LoadingContext);
  const [activeFavorited, setActiveFavorited] = useState<boolean>(true);
  const [activeCommented, setActiveCommented] = useState<boolean>(false);
  const [takeItem, setTakeItem] = useState<number>(8);
  const [activeUrl, setActiveUrl] = useState<string>(`${process.env.NEXT_PUBLIC_SERVER}/api/cultures?saved=1&take=${takeItem}`)
  const [cultures, setCultures] = useState<CultureProps[]>();

  useEffect(() => {
    setLoading(true)
    fetch(activeUrl)
      .then((res) => res.json())
      .then((data) => {
        setCultures(data.cultures)
        setLoading(false)
      })
  }, [activeUrl]);

  useEffect(() => {
    if (activeFavorited) {
      setActiveUrl(`${process.env.NEXT_PUBLIC_SERVER}/api/cultures?saved=1&take=${takeItem}`)
    }
    
    if (activeCommented) {
      setActiveUrl(`${process.env.NEXT_PUBLIC_SERVER}/api/cultures?commented=1&take=${takeItem}`)
    }
  }, [takeItem])

  return (
    <main className="flex flex-col justify-start items-stretch gap-8">
      <Actions takeItem={takeItem} url={{activeUrl, setActiveUrl}} favorited={{ activeFavorited, setActiveFavorited }} commented={{activeCommented, setActiveCommented}} />
      <Items cultures={cultures} />
      <Button variant="secondary" onClick={() => {
        setTakeItem(takeItem + 8)
        }}
      >Load more</Button>
    </main>
  )
}