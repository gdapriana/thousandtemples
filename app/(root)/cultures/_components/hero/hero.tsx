import { CultureProps } from "@/lib/utils"
import { Description } from "@/app/(root)/cultures/_components/hero/_components/description"
import { Card } from "@/app/(root)/cultures/_components/hero/_components/card"

export const Hero = ({ favCultures }: { favCultures: CultureProps[] }) => {
  return (
    <main className="grid grid-cols-1 gap-8 grid-rows-[auto_1fr] md:grid-cols-[1fr_2fr] md:grid-rows-1">
      <Description />
      <div className="grid gap-2 grid-cols-1 grid-rows-3 md:grid-cols-3 md:grid-rows-1">
        {favCultures.map((culture: CultureProps) => {
          return <Card key={culture.id} culture={culture} />
        })}
      </div>
    </main>
  )
}