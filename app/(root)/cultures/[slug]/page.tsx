import { getServerSession } from "next-auth";
import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import { CultureProps } from "@/lib/utils";
import { notFound } from "next/navigation";
import prisma from "@/lib/db";
import { Description } from "@/app/(root)/cultures/[slug]/_components/description";
import { Cover } from "@/app/(root)/cultures/[slug]/_components/cover";
import { Actions } from "@/app/(root)/cultures/[slug]/_components/actions";
import { Comment } from "@/app/(root)/cultures/[slug]/_components/comment";
import { ImagesSlide } from "@/app/(root)/cultures/[slug]/_components/images-slide";
import { Body } from "@/app/(root)/cultures/[slug]/_components/body";
import { Suggestions } from "@/app/(root)/cultures/[slug]/_components/suggestions";

export default async function CulturePage({ params }: { params: { slug: string } }) {
  let favorited = null;
  const session = await getServerSession(authOptions);
  const culture: CultureProps = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/cultures/${params.slug}`, {
    cache: "no-cache",
  })
    .then((res) => res.json())
    .then((data) => data.culture);

  if (!culture) return notFound();

  const favCultures: CultureProps[] = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/cultures?take=4`, {
    cache: "no-cache",
  })
    .then((res) => res.json())
    .then((data) => data.cultures);

  if (session) {
    favorited = await prisma.usersFavoriteCultures.findUnique({
      where: {
        userEmail_cultureSlug: {
          userEmail: session?.user?.email as string,
          cultureSlug: params.slug,
        },
      },
    });
  }
  return (
    <main className="grid grid-cols-1 gap-8 md:grid-cols-2">
      <Description
        title={culture.name}
        description={culture.description}
        address={culture.address}
        category={culture.category}
        district={culture.district}
      />
      <Cover url={culture.cover} />
      <Actions
        session={session}
        slug={params.slug}
        favorite={{ favorited: favorited !== null, count: culture._count.favoritedByUsers }}
        comment={culture._count.commentedByUsers}
      />
      <div className="grid grid-cols-1 gap-8 md:col-span-2 md:grid-cols-[2fr_1fr]">
        <div className="flex flex-col items-stretch justify-start gap-8">
          <Body body={culture.body} />
          <ImagesSlide images={culture.images} />
          <Comment cultureSlug={params.slug} session={session} comments={culture.commentedByUsers} />
        </div>
        <div className="hidden md:flex">
          <Suggestions cultures={favCultures} />
        </div>
      </div>
    </main>
  );
}
