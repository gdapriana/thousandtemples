"use client";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import { Bookmark, Map, MessageCircle } from "lucide-react";
import { ConfirmationDialog } from "@/components/confirmation-dialog";
import { useContext } from "react";
import { LoadingContext } from "@/lib/providers/loading";
import { Session } from "next-auth";

export const Actions = ({
  session,
  slug,
  favorite,
  comment,
}: {
  session: Session | null;
  slug: string;
  favorite: { favorited: boolean; count: number };
  comment: number;
}) => {
  const { setLoading } = useContext(LoadingContext);
  return (
    <main className="flex flex-wrap justify-between gap-2 border-y py-4 md:col-span-2">
      <div className="ms-auto flex items-center justify-center gap-2">
        <Button size="sm" asChild className="gap-1" variant="secondary">
          <Link href="#comment">
            <MessageCircle className="h-4 w-4" />
            {comment} <span className="hidden md:inline">Comment</span>
          </Link>
        </Button>
        {!session && (
          <Button size="sm" className="gap-1" asChild variant="secondary">
            <Link href="/signin">
              <Bookmark className="h-4 w-4" /> Favorite
            </Link>
          </Button>
        )}
        {session && (
          <ConfirmationDialog
            data={{
              title: favorite.favorited ? "Remove from favorite?" : "Add to favorite?",
              subtitle: favorite.favorited
                ? "Are you sure you want to remove?"
                : "Are you sure you want to add to Favorite?",
              button: {
                icon: Bookmark,
                text: favorite.favorited ? `(${favorite.count}) Unfavorite` : `(${favorite.count}) Favorite`,
                variant: favorite.favorited ? "default" : "secondary",
              },
              action: () => {
                setLoading(true);
                if (favorite.favorited) {
                  fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/cultures/${slug}/favorite`, {
                    method: "DELETE",
                    cache: "no-cache",
                  }).then(() => {
                    location.reload();
                    setLoading(false);
                  });
                }
                if (!favorite.favorited) {
                  fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/cultures/${slug}/favorite`, {
                    method: "POST",
                    cache: "no-cache",
                  }).then(() => {
                    location.reload();
                    setLoading(false);
                  });
                }
              },
            }}
          />
        )}
      </div>
    </main>
  );
};
