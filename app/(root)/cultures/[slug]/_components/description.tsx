import { Button } from "@/components/ui/button";
import Link from "next/link";
import { MapPin, SquareStack, Wallet } from "lucide-react";

export const Description = ({
  title,
  description,
  address,
  district,
  category,
}: {
  title: string;
  description: string;
  address?: string;
  district?: { name: string; slug: string };
  category?: { name: string; slug: string };
}) => {
  return (
    <main className="flex flex-col items-stretch justify-center gap-2">
      <h1 className="text-xl font-bold md:text-2xl">{title}</h1>
      <p className="text-sm text-muted-foreground">{description}</p>
      {address && <p className="text-sm font-bold">{address}</p>}
      <div className="mt-2 flex flex-wrap items-center justify-start gap-2">
        {district && (
          <Button asChild size="sm" variant="secondary" className="gap-1">
            <Link href={`/districts/${district?.slug}`}>
              <MapPin className="h-4 w-4" /> {district?.name}
            </Link>
          </Button>
        )}
        {category && (
          <Button asChild size="sm" variant="secondary" className="gap-1">
            <Link href={`/categories/${category?.slug}`}>
              <SquareStack className="h-4 w-4" /> {category?.name}
            </Link>
          </Button>
        )}
      </div>
    </main>
  );
};
