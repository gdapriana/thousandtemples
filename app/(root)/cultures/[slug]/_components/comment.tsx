"use client";
import { cn, UsersCommentCulturesProps, UsersCommentDestinationsProps } from "@/lib/utils";
import { MessageCircle, MessageCirclePlus, MessageCircleWarning, PlusIcon, Trash } from "lucide-react";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import Link from "next/link";
import { Session } from "next-auth";
import { Button } from "@/components/ui/button";
import moment from "moment";
import { ConfirmationDialog } from "@/components/confirmation-dialog";
import { useContext } from "react";
import { LoadingContext } from "@/lib/providers/loading";
import { CommentDialog } from "@/components/comment-dialog";

export const Comment = ({
  session,
  comments,
  cultureSlug,
}: {
  session: Session | null;
  comments: UsersCommentCulturesProps[];
  cultureSlug: string;
}) => {
  const { setLoading } = useContext(LoadingContext);
  return (
    <main id="comment" className="flex flex-col items-stretch justify-start gap-4">
      <header className="flex flex-wrap items-center justify-start gap-1">
        <MessageCircle className="h-6 w-6" /> <h1 className="text-lg font-bold">Comments</h1>
        {!session && (
          <Button asChild className="ms-auto gap-1" variant="secondary">
            <Link href="/signin">
              <PlusIcon className="h-4 w-4" />
              Post a comment
            </Link>
          </Button>
        )}
        {session && (
          <CommentDialog
            data={{
              slug: cultureSlug,
              type: "cultures",
              setLoading: setLoading,
              button: { icon: PlusIcon, text: "Post Comment", variant: "secondary", className: "ms-auto" },
              title: "Post Comment",
              subtitle: "Comment",
              action: () => {},
            }}
          />
        )}
      </header>

      <div className="flex flex-col items-stretch justify-start">
        {comments.length === 0 && (
          <div className="flex flex-col items-center justify-center p-4 text-muted-foreground">
            <MessageCirclePlus className="h-5 w-5" />
            Be first person to comment!
          </div>
        )}
        {comments.map((comment: UsersCommentCulturesProps, index: number) => {
          return (
            <article
              key={index}
              className={cn(
                `flex flex-col items-stretch justify-start gap-4 py-6`,
                index !== comments.length - 1 && "border-b",
              )}
            >
              <header className="flex items-center justify-start">
                <Link href={`/profile/${comment.user.email}`} className="flex items-center justify-center gap-2">
                  <Avatar>
                    <AvatarFallback>{comment.user.name?.charAt(0)}</AvatarFallback>
                    <AvatarImage src={comment.user.image} />
                  </Avatar>
                  <div className="flex flex-col items-start justify-center">
                    <h1 className="font-bold">{comment.user.name}</h1>
                    <p className="text-xs text-muted-foreground">{comment.user.email}</p>
                  </div>
                </Link>
              </header>

              <div className="text-muted-foreground">{comment.body}</div>
              <footer className="flex items-center justify-end gap-2">
                <p className="me-auto text-sm text-muted-foreground">{moment(comment.createdAt).fromNow()}</p>
                {session?.user?.email === comment.userEmail && (
                  <ConfirmationDialog
                    data={{
                      button: { icon: Trash, variant: "secondary" },
                      title: "Delete comments?",
                      subtitle: "Are you sure you want to delete this comments?",
                      action: () => {
                        setLoading(true);
                        fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/cultures/${cultureSlug}/comments/${comment.id}`, {
                          method: "DELETE",
                        }).then(() => {
                          location.reload();
                        });
                      },
                    }}
                  />
                )}
                {session?.user?.email !== comment.userEmail && (
                  <Button size="icon" variant="secondary">
                    <MessageCircleWarning className="h-4 w-4" />
                  </Button>
                )}
              </footer>
            </article>
          );
        })}
      </div>
    </main>
  );
};
