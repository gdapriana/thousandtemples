import { Hero } from "@/app/(root)/cultures/_components/hero/hero";
import { AllItems } from "@/app/(root)/cultures/_components/all-items/all-items";

export default async function CulturesPage() {
  const favCultures = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/cultures?saved=1&take=3`, {
    cache: "no-cache",
  })
    .then((res) => res.json())
    .then((data) => data.cultures);
  return (
    <main className="flex flex-col gap-20 justify-start items-stretch">
      <Hero favCultures={favCultures} />
      <AllItems />
    </main>
  )
}