import { ProfileTabs } from "@/app/(root)/profile/_components/tabs";
import { ProfileAbout } from "@/app/(root)/profile/_components/about";
import { getServerSession } from "next-auth";
import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import { redirect } from "next/navigation";

export default async function ProfilePage() {
  const session = await getServerSession(authOptions);
  if (!session) return redirect("/");

  const userResponse = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/users/${session?.user?.email}`).then(
    (response) => response.json(),
  );

  const user = userResponse.user;
  return (
    <main className="flex flex-col items-stretch justify-start gap-8">
      <ProfileAbout name={session?.user?.name} image={session?.user?.image} email={session?.user?.email} />
      <ProfileTabs
        userEmail={session?.user?.email}
        stories={user.stories}
        favCultures={user.favoritedCultures}
        favDestinations={user.favoritedDestinations}
        favStories={user.favoritedStories}
      />
    </main>
  );
}
