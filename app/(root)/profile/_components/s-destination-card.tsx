import { DestinationProps } from "@/lib/utils";
import { Card, CardContent, CardFooter, CardHeader } from "@/components/ui/card";
import Image from "next/image";
import { Button } from "@/components/ui/button";
import Link from "next/link";

export const SDestinationCard = ({ destination }: { destination: DestinationProps | undefined }) => {
  return (
    <Card className="flex flex-col items-stretch justify-start overflow-hidden">
      <CardHeader className="overflow-hidden p-0">
        <Image
          src={destination?.cover || ""}
          alt="cover"
          width={1920}
          height={1080}
          className="aspect-video object-cover"
        />
      </CardHeader>
      <CardContent className="p-3">
        <h1 className="mb-2 line-clamp-1 font-bold">{destination?.name}</h1>
        <p className="line-clamp-3 text-sm text-muted-foreground">{destination?.description}</p>
      </CardContent>
      <CardFooter className="mt-auto p-3">
        <Button asChild className="ms-auto" size="sm">
          <Link href={`/destinations/${destination?.slug}`} passHref>
            Detail
          </Link>
        </Button>
      </CardFooter>
    </Card>
  );
};
