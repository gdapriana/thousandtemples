"use client";
import { StoryProps } from "@/lib/utils";
import { Card, CardContent, CardFooter, CardHeader } from "@/components/ui/card";
import Image from "next/image";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import { Trash } from "lucide-react";
import { ConfirmationDialog } from "@/components/confirmation-dialog";
import { useContext } from "react";
import { LoadingContext } from "@/lib/providers/loading";
import { useRouter } from "next/navigation";

export const StoryCard = ({ story, userEmail }: { story: StoryProps | undefined; userEmail?: string | null }) => {
  const { loading, setLoading } = useContext(LoadingContext);
  const router = useRouter();

  if (loading) return <div>Loading...</div>;

  return (
    <Card className="flex flex-col items-stretch justify-start overflow-hidden">
      <CardHeader className="overflow-hidden p-0">
        <Image src={story?.cover || ""} alt="cover" width={1920} height={1080} className="aspect-video object-cover" />
      </CardHeader>
      <CardContent className="p-3">
        <h1 className="mb-2 line-clamp-1 font-bold">{story?.name}</h1>
        <p className="line-clamp-3 text-sm text-muted-foreground">{story?.description}</p>
      </CardContent>
      <CardFooter className="mt-auto justify-end gap-1 p-3">
        {userEmail === story?.userEmail && (
          <ConfirmationDialog
            data={{
              button: { icon: Trash },
              title: `Delete Story`,
              subtitle: `Are you sure to delete ${story?.name}?`,
              action: () => {
                fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/stories/${story?.slug}`, { method: "DELETE" }).then(() => {
                  location.reload();
                });
              },
            }}
          />
        )}
        <Button asChild size="sm">
          <Link href={`/stories/${story?.slug}`} passHref>
            Detail
          </Link>
        </Button>
      </CardFooter>
    </Card>
  );
};
