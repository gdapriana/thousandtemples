"use client";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { cn, CultureProps, DestinationProps, StoryProps } from "@/lib/utils";
import { Empty } from "@/app/(root)/profile/_components/empty";
import { StoryCard } from "@/app/(root)/profile/_components/story-card";
import { SStoryCard } from "@/app/(root)/profile/_components/s-story-card";
import { SDestinationCard } from "@/app/(root)/profile/_components/s-destination-card";
import { SCultureCard } from "@/app/(root)/profile/_components/s-culture-card";
import { useEffect } from "react";
import { useRouter } from "next/navigation";

export const ProfileTabs = ({
  userEmail,
  stories,
  favStories,
  favDestinations,
  favCultures,
}: {
  userEmail?: string | null;
  stories: StoryProps[];
  favStories: StoryProps[];
  favDestinations: DestinationProps[];
  favCultures: CultureProps[];
}) => {
  const router = useRouter();
  useEffect(() => {
    router.refresh();
  }, []);
  return (
    <Tabs defaultValue="stories">
      <div className="mb-8 flex w-full items-center justify-start overflow-auto md:justify-center">
        <TabsList className="">
          <TabsTrigger value="stories">Your Stories</TabsTrigger>
          <TabsTrigger value="s_stories">Saved Stories</TabsTrigger>
          <TabsTrigger value="s_destinations">Saved Destinations</TabsTrigger>
          <TabsTrigger value="s_cultures">Saved Cultures</TabsTrigger>
        </TabsList>
      </div>

      <TabsContent
        value="stories"
        className={cn("grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4", stories.length != 0 && "grid")}
      >
        {stories.length === 0 && (
          <Empty title="No Story Found" button={{ name: "Create Story", path: "stories/create" }} />
        )}
        {stories.length !== 0 &&
          stories.map((story: StoryProps) => {
            return <StoryCard userEmail={userEmail} story={story} key={story.id} />;
          })}
      </TabsContent>

      <TabsContent
        value="s_stories"
        className={cn(
          "grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4",
          favStories.length != 0 && "grid",
        )}
      >
        {favStories.length === 0 && (
          <Empty title="Favorited Empty" button={{ name: "Looking for story", path: "/stories" }} />
        )}
        {favStories.length !== 0 &&
          favStories.map((story: any) => {
            return <SStoryCard story={story.story} key={story.story.id} />;
          })}
      </TabsContent>

      <TabsContent
        value="s_destinations"
        className={cn(
          "grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4",
          favDestinations.length != 0 && "grid",
        )}
      >
        {favDestinations.length === 0 && (
          <Empty title="Favorited Empty" button={{ name: "Looking for destination", path: "/destinations" }} />
        )}
        {favDestinations.length !== 0 &&
          favDestinations.map((destination: any) => {
            return <SDestinationCard destination={destination.destination} key={destination.destination.id} />;
          })}
      </TabsContent>

      <TabsContent
        value="s_cultures"
        className={cn(
          "grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4",
          favCultures.length != 0 && "grid",
        )}
      >
        {favCultures.length === 0 && (
          <Empty title="Favorited Empty" button={{ name: "Looking for culture", path: "/cultures" }} />
        )}
        {favCultures.length !== 0 &&
          favCultures.map((culture: any) => {
            return <SCultureCard culture={culture.culture} key={culture.culture.id} />;
          })}
      </TabsContent>
    </Tabs>
  );
};
