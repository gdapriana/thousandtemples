"use client";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { LogOut } from "lucide-react";
import { ConfirmationDialog } from "@/components/confirmation-dialog";
import { signOut } from "next-auth/react";

export const ProfileAbout = ({
  name,
  image,
  email,
}: {
  name: string | undefined | null;
  image: string | undefined | null;
  email: string | undefined | null;
}) => {
  return (
    <main className="flex flex-col items-center justify-start">
      <Avatar>
        <AvatarFallback>{name?.charAt(0)}</AvatarFallback>
        <AvatarImage src={image || ""}></AvatarImage>
      </Avatar>
      <h1 className="mt-4 font-bold">{name}</h1>
      <p className="text-muted-foreground">{email}</p>

      <div className="mt-4 flex items-center justify-center gap-2">
        <ConfirmationDialog
          data={{
            title: "Logout",
            subtitle: "Are you sure to logout?",
            button: { icon: LogOut, text: "Sign Out", variant: "secondary" },
            action: () => {
              signOut();
            },
          }}
        />
      </div>
    </main>
  );
};
