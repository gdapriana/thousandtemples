import Image from "next/image";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import { FileWarningIcon } from "lucide-react";

export const Empty = ({ title, button }: { title: string; button: { name: string; path: string } }) => {
  return (
    <main className="flex w-full flex-col items-center justify-center gap-2 py-20">
      <h1 className="text-sm font-bold">
        <FileWarningIcon className="inline h-4 w-4" /> {title}
      </h1>
      <Button asChild>
        <Link href={button.path}>{button.name}</Link>
      </Button>
    </main>
  );
};
