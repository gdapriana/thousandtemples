import { notFound } from "next/navigation";
import { ProfileAbout } from "@/app/(root)/profile/_components/about";
import { ProfileTabs } from "@/app/(root)/profile/_components/tabs";

export default async function UserPage({ params }: { params: { email: string } }) {
  const response = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/users/${params.email}`, { cache: "no-cache" });
  if (response.status === 404) return notFound();
  const data = await response.json();
  const user = data.user;

  return (
    <main className="flex flex-col items-stretch justify-start gap-8">
      <ProfileAbout name={user?.name} image={user?.image} email={user?.email} />
      <ProfileTabs
        stories={user.stories}
        favCultures={user.favoritedCultures}
        favDestinations={user.favoritedDestinations}
        favStories={user.favoritedStories}
      />
    </main>
  );
}
