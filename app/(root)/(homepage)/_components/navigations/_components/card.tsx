import { LucideIcon } from "lucide-react";
import Link from "next/link";

export const Card = ({
  navigation,
}: {
  navigation: {
    name: string;
    description: string;
    icon: LucideIcon;
    route: string;
  };
}) => {
  return (
    <Link
      href={navigation.route}
      className="flex flex-col items-start justify-start rounded-xl border border-gray-100 p-8 hover:bg-secondary"
    >
      <navigation.icon className="h-10 w-10" />
      <h1 className="mt-4 font-bold md:text-lg">{navigation.name}</h1>
      <p className="line-clamp-2 text-sm text-muted-foreground">{navigation.description}</p>
    </Link>
  );
};
