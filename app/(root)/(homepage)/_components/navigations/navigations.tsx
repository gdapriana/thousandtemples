import { LucideIcon } from "lucide-react";
import { Card } from "@/app/(root)/(homepage)/_components/navigations/_components/card";

export const Navigations = ({
  navigations,
}: {
  navigations: {
    name: string;
    description: string;
    icon: LucideIcon;
    route: string;
  }[];
}) => {
  return (
    <main className="md: grid grid-cols-1 gap-4 md:grid-cols-3">
      {navigations.map(
        (
          navigation: {
            name: string;
            description: string;
            icon: LucideIcon;
            route: string;
          },
          index: number,
        ) => {
          return <Card key={index} navigation={navigation} />;
        },
      )}
    </main>
  );
};
