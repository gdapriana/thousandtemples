import { StoryProps } from "@/lib/utils";
import { Header } from "@/app/(root)/(homepage)/_components/fav-stories/components/header";
import { Card } from "@/app/(root)/(homepage)/_components/fav-stories/components/card";

export const FavStories = ({ stories }: { stories: StoryProps[] }) => {
  return (
    <main className="flex flex-col items-stretch justify-start gap-12 md:flex-row-reverse md:items-center md:justify-center">
      <Header />
      <div className="grid grid-cols-1 gap-4 md:w-2/3 md:grid-cols-3">
        {stories.map((story: StoryProps) => {
          return <Card story={story} />;
        })}
      </div>
    </main>
  );
};
