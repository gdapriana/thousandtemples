import { Button } from "@/components/ui/button";
import Link from "next/link";
import { ArrowUpRight } from "lucide-react";

export const Header = () => {
  return (
    <header className="flex items-center justify-end md:w-1/3">
      <div className="flex flex-col items-end justify-center">
        <h1 className="text-lg font-bold">Top 3 Favorited Stories</h1>
        <p className="text-end text-sm text-muted-foreground">
          Dive into the top 3 stories that captivate the essence of Bali’s culture, history, and local experiences,
          shared by travelers who’ve fallen in love with this beautiful island
        </p>
        <Button asChild className="mt-4 hidden gap-1 md:flex" variant="secondary">
          <Link href="/stories">
            Explore More <ArrowUpRight className="h-5 w-5" />
          </Link>
        </Button>
      </div>
    </header>
  );
};
