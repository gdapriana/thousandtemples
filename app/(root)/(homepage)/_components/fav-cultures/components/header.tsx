import { Button } from "@/components/ui/button";
import Link from "next/link";
import { ArrowUpRight } from "lucide-react";

export const Header = () => {
  return (
    <header className="flex items-center justify-between">
      <div className="flex flex-col items-stretch justify-start">
        <h1 className="text-lg font-bold">Most Favorited Cultures</h1>
        <p className="w-full text-sm text-muted-foreground md:w-1/2">
          Discover the 4 most favored destinations in Bali, where stunning landscapes, rich culture, and unforgettable
          experiences come together to create a paradise for travelers.
        </p>
      </div>
      <Button asChild className="hidden gap-1 md:flex" variant="secondary">
        <Link href="/destinations">
          Explore More <ArrowUpRight className="h-5 w-5" />
        </Link>
      </Button>
    </header>
  );
};
