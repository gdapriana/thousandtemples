import { Button } from "@/components/ui/button";
import Link from "next/link";
import { ArrowUpRight } from "lucide-react";
import { CultureProps, DestinationProps } from "@/lib/utils";
import { Card } from "@/app/(root)/(homepage)/_components/fav-cultures/components/card";
import { Header } from "@/app/(root)/(homepage)/_components/fav-cultures/components/header";

export const FavCultures = ({ cultures }: { cultures: CultureProps[] }) => {
  return (
    <main className="flex flex-col items-stretch justify-start gap-8">
      <Header />
      <div className="md:hidden"></div>
      <div className="hidden grid-cols-2 gap-4 md:grid lg:grid-cols-4">
        {cultures.map((culture: CultureProps) => {
          return <Card culture={culture} key={culture.id} />;
        })}
      </div>
      <Button size="sm" asChild className="gap-1 md:hidden" variant="secondary">
        <Link href="/destinations">
          Explore More <ArrowUpRight className="h-5 w-5" />
        </Link>
      </Button>
    </main>
  );
};
