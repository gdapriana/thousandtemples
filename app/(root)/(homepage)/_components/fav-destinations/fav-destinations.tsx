import { Button } from "@/components/ui/button";
import Link from "next/link";
import { ArrowUpRight, Car } from "lucide-react";
import { DestinationProps } from "@/lib/utils";
import { Card } from "@/app/(root)/(homepage)/_components/fav-destinations/components/card";
import { Header } from "@/app/(root)/(homepage)/_components/fav-destinations/components/header";

export const FavDestinations = ({ destinations }: { destinations: DestinationProps[] }) => {
  return (
    <main className="flex flex-col items-stretch justify-start gap-8">
      <Header />
      <div className="md:hidden"></div>
      <div className="hidden grid-cols-2 gap-4 md:grid lg:grid-cols-4">
        {destinations.map((destination: DestinationProps) => {
          return <Card mode="desktop" destination={destination} key={destination.id} />;
        })}
      </div>
      <div className="md:hidden flex overflow-auto gap-4">
        {destinations.map((destination: DestinationProps, index: number) => {
          return <Card destination={destination} key={destination.id} mode="mobile" />
        })}
      </div>
      <Button size="sm" asChild className="gap-1 md:hidden" variant="secondary">
        <Link href="/destinations">
          Explore More <ArrowUpRight className="h-5 w-5" />
        </Link>
      </Button>
    </main>
  );
};
