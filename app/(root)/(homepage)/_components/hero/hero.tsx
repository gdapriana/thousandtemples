import { Brand } from "@/app/(root)/_components/header/_components/brand";
import { Button } from "@/components/ui/button";
import { metadata } from "@/lib/metadata";
import Image from "next/image";
import Link from "next/link";

export const Hero = () => {
  return (
    <main className="h-[400px] relative flex justify-center items-center">
      <Image src="/hero-cover.jpg" width={1920} height={1080} alt="hero-cover" className="absolute object-top top-0 left-0 w-full h-full object-cover brightness-75 rounded-2xl" />
      <div className="flex z-10 flex-col justify-center items-center">
        <Brand link="/logo.png" text={metadata.name} className={{text: 'md:text-[2rem] text-[1.2rem] text-secondary inline', wrapper: "flex-col", image: 'w-20 h-20'}} />
        <p className="text-primary-foreground text-sm md:text-base text-center">{metadata.highlight}</p>
      <Button asChild className="mt-6 font-bold" size="lg" variant="outline">
        <Link href={'#'}>Explore Now</Link>
      </Button>
      </div>
    </main>
  );
};
