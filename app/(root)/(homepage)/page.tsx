import { Hero } from "@/app/(root)/(homepage)/_components/hero/hero";
import { Navigations } from "@/app/(root)/(homepage)/_components/navigations/navigations";
import { metadata } from "@/lib/metadata";
import { FavDestinations } from "@/app/(root)/(homepage)/_components/fav-destinations/fav-destinations";
import { FavStories } from "@/app/(root)/(homepage)/_components/fav-stories/fav-stories";
import { FavCultures } from "@/app/(root)/(homepage)/_components/fav-cultures/fav-cultures";

export default async function Home() {
  const favDestinations = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/destinations?take=4`, {
    cache: "no-cache",
  })
    .then((res) => res.json())
    .then((data) => data.destinations);

  const favStories = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/stories?saved=1&take=3`, { cache: "no-cache" })
    .then((res) => res.json())
    .then((data) => data.stories);

  const favCultures = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/cultures?take=4`, { cache: "no-cache" })
    .then((res) => res.json())
    .then((data) => data.cultures);

  return (
    <main className="flex flex-col items-stretch justify-start gap-10">
      <Hero />
      <Navigations navigations={metadata.navigations} />
      <FavStories stories={favStories} />
      <FavDestinations destinations={favDestinations} />
      <FavCultures cultures={favCultures} />
    </main>
  );
}
