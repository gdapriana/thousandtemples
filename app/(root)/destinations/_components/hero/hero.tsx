import { DestinationProps } from "@/lib/utils"
import { Description } from "@/app/(root)/destinations/_components/hero/_components/description"
import { Card } from "@/app/(root)/destinations/_components/hero/_components/card"

export const Hero = ({ favDestinations }: { favDestinations: DestinationProps[] }) => {
  return (
    <main className="grid grid-cols-1 gap-8 grid-rows-[auto_1fr] md:grid-cols-[1fr_2fr] md:grid-rows-1">
      <Description />
      <div className="grid gap-2 grid-cols-1 grid-rows-3 md:grid-cols-3 md:grid-rows-1">
        {favDestinations.map((destination: DestinationProps) => {
          return <Card mode="desktop" key={destination.id} destination={destination} />
        })}
      </div>
    </main>
  )
}