import { metadata } from "@/lib/metadata"
import { Search } from "lucide-react"

export const Description = () => {
  return (
    <main className="flex flex-col justify-center gap-2 items-start">
      <h1 className="text-xl md:text-2xl font-bold">
        {metadata.navigations[1].name}</h1>
      <p className="text-muted-foreground">{metadata.navigations[1].description}</p>
      <div className="flex mt-4 gap-2 px-4 justify-center items-center border overflow-hidden rounded-lg">
        <input type="text" placeholder="Search destinations..." className="py-2 border-none outline-none" />
        <Search className="w-4 h-4" />
      </div>
    </main>
  )
}