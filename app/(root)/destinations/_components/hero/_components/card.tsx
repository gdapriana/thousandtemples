import { cn, DestinationProps } from "@/lib/utils";
import Link from "next/link";
import Image from "next/image";
import { Button } from "@/components/ui/button";
import { MapPin, SquareStack } from "lucide-react";

export const Card = ({ destination, mode }: { destination: DestinationProps; mode: "mobile" | "desktop" }) => {
  return (
    <Link
      href={`/destinations/${destination.slug}`}
      className={cn(`flex flex-col items-stretch justify-start rounded-2xl p-2 hover:bg-secondary`, mode === "mobile" ? "w-[400px]" : "w-auto")}
    >
      <Image
        src={destination.cover || ""}
        alt="cover"
        width={1920}
        height={1080}
        className="aspect-video rounded-xl object-cover"
      />
      <div className="flex flex-col items-stretch justify-start gap-2 p-2">
        <h1 className="mt-2 line-clamp-1 font-bold">{destination.name}</h1>
        <p className="line-clamp-2 text-sm text-muted-foreground">{destination.description}</p>
      </div>
    </Link>
  );
};
