import { DestinationProps, StoryProps } from "@/lib/utils"
import { Card } from "@/app/(root)/destinations/_components/all-items/_components/card";

export const Items = ({ destinations }: { destinations: DestinationProps[] | undefined }) => {
  return (
    <main className="grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
      {destinations?.map((destination: DestinationProps) => {
        return <Card destination={destination} />
      })}
    </main>
  )
}