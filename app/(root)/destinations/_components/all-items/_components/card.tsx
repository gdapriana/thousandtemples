import { cn, DestinationProps } from "@/lib/utils";
import Link from "next/link";
import Image from "next/image";
import { Button } from "@/components/ui/button";
import { Bookmark, MapPin, MessageCircle, SquareStack } from "lucide-react";

export const Card = ({ destination }: { destination: DestinationProps }) => {
  return (
    <Link
      href={`/destinations/${destination.slug}`}
      className={cn(`flex flex-col items-stretch justify-start rounded-2xl p-2 hover:bg-secondary`)}
    >
      <Image
        src={destination.cover || ""}
        alt="cover"
        width={1920}
        height={1080}
        className="aspect-video rounded-xl object-cover"
      />
      <div className="flex flex-col items-stretch justify-start gap-2 p-2">
        <h1 className="mt-2 line-clamp-1 font-bold">{destination.name}</h1>
        <p className="line-clamp-2 text-sm text-muted-foreground">{destination.description}</p>
        <div className="mt-4 flex items-center justify-end gap-2">
          <div className="flex text-muted-foreground justify-center items-center gap-1">
            <Bookmark className="w-4 h-4" />
            <span className="text-sm">{destination._count.favoritedByUsers}</span>
          </div>
          <div className="flex text-muted-foreground justify-center items-center gap-1">
            <MessageCircle className="w-4 h-4" />
            <span className="text-sm">{destination._count.commentedByUsers}</span>
          </div>
        </div>
      </div>
    </Link>
  );
};
