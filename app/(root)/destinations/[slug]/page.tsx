import { notFound } from "next/navigation";
import { DestinationProps } from "@/lib/utils";
import { Description } from "@/app/(root)/destinations/[slug]/_components/description";
import { Cover } from "@/app/(root)/destinations/[slug]/_components/cover";
import { Actions } from "@/app/(root)/destinations/[slug]/_components/actions";
import prisma from "@/lib/db";
import { getServerSession } from "next-auth";
import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import { Comment } from "@/app/(root)/destinations/[slug]/_components/comment";
import { ImagesSlide } from "@/app/(root)/destinations/[slug]/_components/images-slide";
import Map from "@/app/(root)/destinations/[slug]/_components/map";
import { Suggestions } from "@/app/(root)/destinations/[slug]/_components/suggestions";
import { Button } from "@/components/ui/button";

export default async function DestinationPage({ params }: { params: { slug: string } }) {
  let favorited = null;
  const session = await getServerSession(authOptions);
  const destination: DestinationProps = await fetch(
    `${process.env.NEXT_PUBLIC_SERVER}/api/destinations/${params.slug}`,
    { cache: "no-cache" },
  )
    .then((res) => res.json())
    .then((data) => data.destination);
  if (!destination) return notFound();

  const favDestination: DestinationProps[] = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/destinations?take=4`, {
    cache: "no-cache",
  })
    .then((res) => res.json())
    .then((data) => data.destinations);

  if (session) {
    favorited = await prisma.usersFavoriteDestinations.findUnique({
      where: {
        userEmail_destinationSlug: {
          userEmail: session?.user?.email as string,
          destinationSlug: params.slug,
        },
      },
    });
  }

  return (
    <main className="grid grid-cols-1 gap-8 md:grid-cols-2">
      <Description
        title={destination.name}
        description={destination.description}
        address={destination.address}
        category={{ name: destination.category.name, slug: destination.category.slug }}
        district={{ name: destination.district.name, slug: destination.district.slug }}
        entry={destination.price}
      />
      <Cover url={destination.cover} />
      <Actions
        session={session}
        slug={destination.slug}
        map="https://google.com"
        comment={destination._count.commentedByUsers}
        favorite={{ favorited: favorited !== null, count: destination._count.favoritedByUsers }}
      />
      <div className="grid grid-cols-1 gap-8 md:col-span-2 md:grid-cols-[2fr_1fr]">
        <div className="flex flex-col items-stretch justify-start gap-8">
          <ImagesSlide images={destination.images} />
          <Map />
          <Comment destinationSlug={params.slug} session={session} comments={destination.commentedByUsers} />
        </div>
        <div className="hidden md:flex">
          <Suggestions destinations={favDestination} />
        </div>
      </div>
    </main>
  );
}
