import { cn, ImageProps } from "@/lib/utils";
import { HeartCrack, Images } from "lucide-react";
import Image from "next/image";
import { OpenImage } from "@/app/(root)/destinations/[slug]/_actions/open-image";

export const ImagesSlide = ({ images }: { images: ImageProps[] }) => {
  return (
    <main className="flex flex-col items-stretch justify-start gap-4">
      <header className="flex items-center justify-start gap-1">
        <Images className="inline h-6 w-6" /> <h1 className="inline text-lg font-bold">Additional Images</h1>
      </header>
      <div
        className={cn(
          "",
          images.length !== 0
            ? "grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4"
            : "flex items-center justify-center p-8",
        )}
      >
        {images.length === 0 && (
          <h1 className="flex flex-col items-center justify-center gap-1 text-muted-foreground">
            <HeartCrack className="h-5 w-5" /> No Additional Image Found
          </h1>
        )}

        {images.length !== 0 &&
          images.map((image: ImageProps) => {
            return <OpenImage image={image} />;
          })}
      </div>
    </main>
  );
};
