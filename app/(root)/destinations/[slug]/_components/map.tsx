import { MapIcon, Smile } from "lucide-react";

export default function Map() {
  return (
    <main className="flex flex-col items-stretch justify-start gap-4">
      <header>
        <MapIcon className="inline h-6 w-6" /> <h1 className="inline text-lg font-bold">Maps</h1>
      </header>
      <div className="flex items-center justify-center">
        <div className="flex flex-col items-center justify-center text-muted-foreground">
          <Smile className="h-5 w-5" />
          Map available soon!
        </div>
      </div>
    </main>
  );
}
