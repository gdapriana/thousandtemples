import { DestinationProps } from "@/lib/utils";
import Image from "next/image";
import Link from "next/link";

export const Suggestions = ({ destinations }: { destinations: DestinationProps[] }) => {
  return (
    <main className="flex flex-col items-stretch justify-start gap-4">
      <h1 className="font-bold">Most Favorited Destinations</h1>
      <div className="flex flex-col items-stretch justify-start gap-4">
        {destinations.map((destination: DestinationProps) => {
          return (
            <Link
              href={`/destinations/${destination.slug}`}
              key={destination.id}
              className="flex items-start justify-center gap-4 rounded-2xl p-2 hover:bg-gray-100"
            >
              <Image
                src={destination.cover || ""}
                alt="cover"
                width={1000}
                height={1000}
                className="aspect-square w-20 rounded-xl object-cover"
              />
              <div className="flex flex-1 flex-col items-start justify-start gap-1">
                <h1 className="line-clamp-1 font-semibold">{destination.name}</h1>
                <p className="line-clamp-2 text-sm text-muted-foreground">{destination.description}</p>
              </div>
            </Link>
          );
        })}
      </div>
    </main>
  );
};
