import { Hero } from "@/app/(root)/destinations/_components/hero/hero";
import { AllItems } from "./_components/all-items/all-items";

export default async function DestinationsPage() {
  const favDestinations = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/destinations?saved=1&take=3`, {
    cache: "no-cache",
  })
    .then((res) => res.json())
    .then((data) => data.destinations);
  return (
    <main className="flex flex-col gap-20 justify-start items-stretch">
      <Hero favDestinations={favDestinations} />
      <AllItems />
    </main>
  )
}