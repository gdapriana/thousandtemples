import { Sheet, SheetContent, SheetTrigger } from "@/components/ui/sheet";
import { ChevronDown } from "lucide-react";
import Link from "next/link";
import { Button } from "@/components/ui/button";

export const Hamburger = ({ navigations }: { navigations: { name: string; path: string }[] }) => {
  return (
    <Sheet>
      <SheetTrigger className="ms-auto sm:hidden">
        <ChevronDown className="h-6 w-6" />
      </SheetTrigger>
      <SheetContent side="top">
        {navigations.map((route: { name: string; path: string }, index: number) => {
          return (
            <Link href={route.path} key={index}>
              <Button variant="link">{route.name}</Button>
            </Link>
          );
        })}
      </SheetContent>
    </Sheet>
  );
};
