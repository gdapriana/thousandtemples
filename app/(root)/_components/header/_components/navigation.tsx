import { Button } from "@/components/ui/button";
import Link from "next/link";

export const Navigation = ({ navigations }: { navigations: { name: string; path: string }[] }) => {
  return (
    <main className="ms-auto hidden items-center justify-center sm:flex">
      {navigations.map((route: { name: string; path: string }, index: number) => {
        return (
          <Button key={index} asChild size="sm" variant="ghost">
            <Link href={route.path}>{route.name}</Link>
          </Button>
        );
      })}
    </main>
  );
};
