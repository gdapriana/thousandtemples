import Link from "next/link";
import Image from "next/image";
import { cn } from "@/lib/utils";

export const Brand = ({ link, text, className }: { link: string; text: string; className?: {wrapper?: string; image?: string; text?: string} }) => {
  return (
    <Link className={cn("flex items-center justify-center gap-2", className?.wrapper)} href={"/"}>
      <Image src={link} alt="logo" width={1000} height={1000} className={cn("h-12 w-12", className?.image)} />
      <h1 className={cn("hidden text-lg font-bold md:inline", className?.text)}>{text}</h1>
    </Link>
  );
};
