import { Session } from "next-auth";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { useContext } from "react";
import { LoadingContext } from "@/lib/providers/loading";

export const Profile = ({
  session,
  status,
}: {
  session: Session | null;
  status: "loading" | "authenticated" | "unauthenticated";
}) => {
  const { setLoading } = useContext(LoadingContext);
  if (status === "loading") setLoading(true);
  if (status !== "loading") setLoading(false);

  if (status === "unauthenticated") {
    return (
      <Button asChild size="sm">
        <Link href="/signin">Sign in</Link>
      </Button>
    );
  }

  return (
    <Link href="/profile">
      <Avatar>
        <AvatarFallback>{session?.user?.name?.charAt(0)}</AvatarFallback>
        <AvatarImage src={session?.user?.image || ""} />
      </Avatar>
    </Link>
  );
};
