import { cn } from "@/lib/utils";
import { metadata } from "@/lib/metadata";
import { Brand } from "@/app/(root)/_components/header/_components/brand";
import { Navigation } from "@/app/(root)/_components/header/_components/navigation";
import { Profile } from "@/app/(root)/_components/header/_components/profile";
import { useSession } from "next-auth/react";
import { Hamburger } from "@/app/(root)/_components/header/_actions/hamburger";

export const Header = ({ scrolled }: { scrolled: boolean }) => {
  const { data, status } = useSession();
  return (
    <header className={cn("flex items-center justify-center", scrolled && "border-b")}>
      <div className="flex w-full max-w-6xl items-center justify-between gap-2 p-4">
        <Brand link={metadata.logo} text={metadata.name} />
        <Navigation navigations={metadata.routes} />
        <Hamburger navigations={metadata.routes} />
        <Profile session={data} status={status} />
      </div>
    </header>
  );
};
