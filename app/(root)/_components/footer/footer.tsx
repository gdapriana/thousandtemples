import { Brand } from "@/app/(root)/_components/header/_components/brand";

import { metadata } from "@/lib/metadata";
import { Links } from "@/app/(root)/_components/footer/_components/links";

export const Footer = () => {
  return (
    <footer className="flex w-full items-center justify-center border-t">
      <div className="flex w-full max-w-6xl flex-col gap-4 p-4 md:flex-row md:gap-12">
        <div className="flex w-full flex-col items-start justify-start gap-4 md:w-auto md:flex-1">
          <Brand link={metadata.logo} text={metadata.name} />
          <p className="text-sm text-muted-foreground md:w-2/3">{metadata.description}</p>
        </div>
        <div className="w-full md:w-auto">
          <Links links={metadata.routes} header="Navigate To" />
        </div>
        <div className="w-full md:w-auto">
          <Links links={metadata.suports} header="Other" />
        </div>
      </div>
    </footer>
  );
};
