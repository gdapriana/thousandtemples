import { Button } from "@/components/ui/button";
import Link from "next/link";

export const Links = ({ links, header }: { links: { name: string; path: string }[]; header: string }) => {
  return (
    <main className="flex flex-col items-stretch justify-start gap-2">
      <header className="font-bold">{header}</header>
      <div className="flex flex-col items-start justify-start">
        {links.map((link: { name: string; path: string }, index: number) => {
          return (
            <Button className="p-0" asChild variant="link">
              <Link href={link.path}>{link.name}</Link>
            </Button>
          );
        })}
      </div>
    </main>
  );
};
