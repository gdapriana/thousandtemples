"use client";

import { useContext } from "react";
import { ScrollContext } from "@/lib/providers/scroll";
import { LoadingContext } from "@/lib/providers/loading";
import { Header } from "@/app/(root)/_components/header/header";
import { Footer } from "@/app/(root)/_components/footer/footer";

export default function RootLayout({ children }: { children: React.ReactNode }) {
  const { scrolled, setScrolled } = useContext(ScrollContext);
  const { loading } = useContext(LoadingContext);
  const scrollHandle = (event: any) => setScrolled(event.target.scrollTop > 0);

  return (
    <main className="flex h-screen flex-col items-stretch justify-start">
      {loading && (
        <main className="absolute left-0 top-0 flex h-screen w-full items-center justify-center bg-white">
          Loading...
        </main>
      )}
      <Header scrolled={scrolled} />
      <article
        className="flex grow basis-0 flex-col items-center justify-start gap-12 overflow-auto"
        onScrollCapture={scrollHandle}
      >
        <div className="w-full max-w-6xl p-4">{children}</div>
        <Footer />
      </article>
    </main>
  );
}
