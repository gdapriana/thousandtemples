import { ReactNode } from "react";
import { getServerSession } from "next-auth";
import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import { redirect } from "next/navigation";

export default async function PostCreateLayout({ children }: { children: ReactNode }) {
  const session = await getServerSession(authOptions);
  if (!session) return redirect("/signin");
  return <main>{children}</main>;
}
