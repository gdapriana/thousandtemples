import { PostForm } from "@/app/(root)/stories/create/_components/post-form";

export default function CreateStoryPage() {
  return <PostForm />;
}
