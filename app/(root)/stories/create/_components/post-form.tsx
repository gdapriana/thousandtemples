"use client";
import { z } from "zod";

import { Button } from "@/components/ui/button";
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from "@/components/ui/form";
import { Input } from "@/components/ui/input";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { Textarea } from "@/components/ui/textarea";
import { ImagePreview } from "@/app/(root)/stories/create/_components/image-preview";
import { ChangeEvent, useState } from "react";

const formSchema = z.object({
  name: z.string().min(3).max(100),
  description: z.string().min(20).max(200),
  body: z.string().min(10).max(200),
  readTime: z.string().optional(),
  cover: z.string().url(),
});

export const PostForm = () => {
  const [coverUrl, setCoverUrl] = useState<string>("");

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: "",
      description: "",
      readTime: "",
      cover: "",
    },
  });

  function onSubmit(values: z.infer<typeof formSchema>) {
    console.log(values);
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="mt-4 grid w-full grid-cols-1 gap-8 md:grid-cols-2">
        <div className="flex flex-col items-stretch justify-start gap-6">
          <FormField
            control={form.control}
            name="name"
            render={({ field }) => (
              <FormItem className="flex flex-col items-start justify-center">
                <FormLabel>Name</FormLabel>
                <FormControl>
                  <Input placeholder="Your story name..." {...field} />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="description"
            render={({ field }) => (
              <FormItem className="flex flex-col items-start justify-center">
                <FormLabel>Description</FormLabel>
                <FormControl>
                  <Textarea placeholder="Your story description" {...field} />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="readTime"
            render={({ field }) => (
              <FormItem className="flex flex-col items-start justify-center">
                <FormLabel>Read Timte (Minutes)</FormLabel>
                <FormControl>
                  <Input type="number" placeholder="Your story read times..." {...field} />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="cover"
            render={({ field }) => (
              <FormItem className="flex flex-col items-start justify-center">
                <FormLabel>Story Cover</FormLabel>
                <FormControl className="flex items-center justify-center">
                  <div className="flex w-full items-center justify-center gap-1">
                    <Input
                      onChangeCapture={(e: ChangeEvent<HTMLInputElement>) => {
                        setCoverUrl(e.target.value);
                        console.log(coverUrl);
                      }}
                      className="flex-1"
                      type="text"
                      placeholder="Your story cover url..."
                      {...field}
                    />
                    <Button type="button">Show Preview</Button>
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <ImagePreview key={coverUrl} imageUrl={coverUrl} />
        </div>
        <div className="w-1/2">body</div>
        <div className="flex items-center justify-end md:col-span-2">
          <Button size="sm" type="submit" className="">
            Post
          </Button>
        </div>
      </form>
    </Form>
  );
};
