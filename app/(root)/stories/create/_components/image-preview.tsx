"use client";
import Image from "next/image";
import { Dispatch, SetStateAction, useState } from "react";

export const ImagePreview = ({ imageUrl }: { imageUrl: string }) => {
  const [validImage, setValidImage] = useState<boolean>(true);
  const validHanddle = () => {
    setValidImage(!validImage);
  };
  if (validImage) {
    return (
      <Image
        src={imageUrl}
        unoptimized
        alt="cover"
        onError={validHanddle}
        width={1000}
        height={1000}
        className="w-full"
      />
    );
  } else {
    return <p>Error</p>;
  }
};
