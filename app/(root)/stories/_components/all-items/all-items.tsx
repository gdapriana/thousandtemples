'use client'
import { Button } from "@/components/ui/button"
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "@/components/ui/select"
import { useContext, useEffect, useState } from "react"
import { Actions } from "@/app/(root)/stories/_components/all-items/_components/actions"
import { StoryProps } from "@/lib/utils"
import { LoadingContext } from "@/lib/providers/loading"
import { Items } from "./_components/items"

export const AllItems = () => {

  const { setLoading } = useContext(LoadingContext);
  const [activeFavorited, setActiveFavorited] = useState<boolean>(true);
  const [activeCommented, setActiveCommented] = useState<boolean>(false);
  const [takeItem, setTakeItem] = useState<number>(8);
  const [activeUrl, setActiveUrl] = useState<string>(`${process.env.NEXT_PUBLIC_SERVER}/api/stories?saved=1`)
  const [stories, setStories] = useState<StoryProps[]>();

  useEffect(() => {
    setLoading(true)
    fetch(activeUrl)
      .then((res) => res.json())
      .then((data) => {
        setStories(data.stories)
        setLoading(false)
      })
  }, [activeUrl]);
  return (
    <main className="flex flex-col justify-start items-stretch gap-8">
      <Actions url={{activeUrl, setActiveUrl}} favorited={{ activeFavorited, setActiveFavorited }} commented={{activeCommented, setActiveCommented}} />
      <Items stories={stories} />
      <Button variant="secondary" onClick={() => {
        setTakeItem(takeItem + 8)
        }}
      >Load more</Button>
    </main>
  )
}