import { Button } from "@/components/ui/button";
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "@/components/ui/select";
import { Dispatch, SetStateAction } from "react";

export const Actions = ({
  favorited,
  commented,
  url
}: {
  favorited: {activeFavorited: boolean; setActiveFavorited: Dispatch<SetStateAction<boolean>>}
  commented: {activeCommented: boolean; setActiveCommented: Dispatch<SetStateAction<boolean>>}
  url: {activeUrl: string; setActiveUrl: Dispatch<SetStateAction<string>>}
}) => {
  return (
    <main className="py-4 border-y flex flex-wrap justify-end items-center gap-4">
      <div className="flex justify-center items-center gap-2">
        <Button variant={favorited.activeFavorited ? "default" : "secondary"} onClick={() => {
          favorited.setActiveFavorited(!favorited.activeFavorited)
          commented.setActiveCommented(!commented.activeCommented)
          url.setActiveUrl(`${process.env.NEXT_PUBLIC_SERVER}/api/stories?saved=1`)
        }}>Most Favorited</Button>
        <Button variant={commented.activeCommented ? "default" : "secondary"} onClick={() => {
          commented.setActiveCommented(!commented.activeCommented)
          favorited.setActiveFavorited(!favorited.activeFavorited)
          url.setActiveUrl(`${process.env.NEXT_PUBLIC_SERVER}/api/stories?commented=1`)
        }}>Most Commented</Button>
      </div>
    </main>
  )
}