import { StoryProps } from "@/lib/utils"
import { Card } from "@/app/(root)/stories/_components/all-items/_components/card";

export const Items = ({ stories }: { stories: StoryProps[] | undefined }) => {
  return (
    <main className="grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
      {stories?.map((story: StoryProps) => {
        return <Card story={story} key={story.id} />
      })}
    </main>
  )
}