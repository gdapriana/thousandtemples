import Link from "next/link";
import { StoryProps } from "@/lib/utils";
import Image from "next/image";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";

export const Card = ({ story }: { story: StoryProps }) => {
  return (
    <Link
      className="relative flex h-[400px] flex-1 flex-col items-stretch justify-end gap-4 overflow-hidden rounded-2xl"
      href={`/stories/${story.slug}`}
    >
      <div className="z-20 m-3 flex flex-col items-stretch justify-start gap-4 rounded-xl bg-primary-foreground p-3">
        <div className="flex flex-col items-stretch justify-start">
          <h1 className="line-clamp-1 text-sm font-bold">{story.name}</h1>
          <p className="line-clamp-2 text-xs text-muted-foreground">{story.description}</p>
        </div>
        <div className="flex items-center justify-center gap-2">
          <Avatar>
            <AvatarFallback>{story.user.name?.charAt(0)}</AvatarFallback>
            <AvatarImage src={story.user.image} />
          </Avatar>
          <div className="flex flex-1 flex-col items-start justify-center overflow-auto">
            <p className="line-clamp-1 text-xs font-semibold">{story.user.name}</p>
            <p className="line-clamp-1 text-xs text-muted-foreground">{story.user.email}</p>
          </div>
        </div>
      </div>
      <Image
        src={story.cover || ""}
        alt="cover"
        width={1000}
        height={1920}
        className="absolute z-10 h-full w-full object-cover"
      />
    </Link>
  );
};
