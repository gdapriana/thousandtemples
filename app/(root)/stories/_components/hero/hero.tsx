import { StoryProps } from "@/lib/utils"
import { Description } from "@/app/(root)/stories/_components/hero/_components/description"
import { Card } from "@/app/(root)/stories/_components/hero/_components/card"

export const Hero = ({ favStories }: { favStories: StoryProps[] }) => {
  return (
    <main className="grid grid-cols-1 gap-8 grid-rows-[auto_1fr] md:grid-cols-[1fr_2fr] md:grid-rows-1">
      <Description />
      <div className="grid gap-2 grid-cols-1 grid-rows-3 md:grid-cols-3 md:grid-rows-1">
        {favStories.map((story: StoryProps) => {
          return <Card key={story.id} story={story} />
        })}
      </div>
    </main>
  )
}