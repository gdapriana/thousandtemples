import { getServerSession } from "next-auth";
import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import { CultureProps, StoryProps } from "@/lib/utils";
import { notFound } from "next/navigation";
import { Description } from "@/app/(root)/stories/[slug]/_components/description";
import { Cover } from "@/app/(root)/stories/[slug]/_components/cover";
import { Actions } from "@/app/(root)/stories/[slug]/_components/actions";
import prisma from "@/lib/db";
import { Body } from "@/app/(root)/stories/[slug]/_components/body";
import { ImagesSlide } from "@/app/(root)/stories/[slug]/_components/images-slide";
import { Comment } from "@/app/(root)/stories/[slug]/_components/comment";
import { Suggestions } from "@/app/(root)/stories/[slug]/_components/suggestions";

export default async function StoryPage({ params }: { params: { slug: string } }) {
  let favorited = null;
  const session = await getServerSession(authOptions);

  const story: StoryProps = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/stories/${params.slug}`, {
    cache: "no-cache",
  })
    .then((res) => res.json())
    .then((data) => data.story);

  if (!story) return notFound();

  const favStories: StoryProps[] = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/stories?take=4`, {
    cache: "no-cache",
  })
    .then((res) => res.json())
    .then((data) => data.stories);

  if (session) {
    favorited = await prisma.usersFavoriteStories.findUnique({
      where: {
        userEmail_storySlug: {
          userEmail: session?.user?.email as string,
          storySlug: params.slug,
        },
      },
    });
  }

  return (
    <main className="grid grid-cols-1 gap-8 md:grid-cols-2">
      <Description createdAt={story.createdAt} title={story.name} description={story.description} user={story.user} />
      <Cover url={story.cover} />
      <Actions
        name={story.name}
        readTime={story.readtime}
        session={session}
        user={story.user}
        slug={params.slug}
        favorite={{ favorited: favorited !== null, count: story._count.favoritedByUsers }}
        comment={story._count.commentedByUsers}
      />
      <div className="grid grid-cols-1 gap-8 md:col-span-2 md:grid-cols-[2fr_1fr]">
        <div className="flex flex-col items-stretch justify-start gap-8">
          <Body body={story.body} />
          <ImagesSlide images={story.images} />
          <Comment session={session} comments={story.commentedByUsers} storySlug={params.slug} />
        </div>
        <div className="hidden md:flex">
          <Suggestions stories={favStories} />
        </div>
      </div>
    </main>
  );
}
