import { ImageProps } from "@/lib/utils";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import Image from "next/image";

export const OpenImage = ({ image }: { image: ImageProps }) => {
  return (
    <Dialog>
      <DialogTrigger>
        <Image
          src={image.uri || ""}
          alt="Aditional Image"
          width={1920}
          height={1080}
          className="aspect-video rounded-xl object-cover"
        />
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Additional Image</DialogTitle>
        </DialogHeader>
        <DialogDescription>{image?.description}</DialogDescription>
        <Image
          src={image.uri || ""}
          alt="Aditional Image"
          width={1920}
          height={1080}
          className="aspect-video w-full max-w-3xl rounded-xl object-cover"
        />
      </DialogContent>
    </Dialog>
  );
};
