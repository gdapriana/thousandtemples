import Image from "next/image";

export const Cover = ({ url }: { url: string | undefined }) => {
  return (
    <div className="overflow-hidden rounded-xl">
      <Image src={url || ""} alt="cover" width={1920} height={1080} className="h-full w-full object-cover" />
    </div>
  );
};
