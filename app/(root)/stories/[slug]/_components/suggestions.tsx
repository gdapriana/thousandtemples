import { CultureProps, StoryProps } from "@/lib/utils";
import Image from "next/image";
import Link from "next/link";

export const Suggestions = ({ stories }: { stories: StoryProps[] }) => {
  return (
    <main className="flex flex-col items-stretch justify-start gap-4">
      <h1 className="font-bold">Most Favorited Stories</h1>
      <div className="flex flex-col items-stretch justify-start gap-4">
        {stories.map((story: StoryProps) => {
          return (
            <Link
              href={`/stories/${story.slug}`}
              key={story.id}
              className="flex items-start justify-center gap-4 rounded-2xl p-2 hover:bg-gray-100"
            >
              <Image
                src={story.cover || ""}
                alt="cover"
                width={1000}
                height={1000}
                className="aspect-square w-20 rounded-xl object-cover"
              />
              <div className="flex flex-1 flex-col items-start justify-start gap-1">
                <h1 className="line-clamp-1 font-semibold">{story.name}</h1>
                <p className="line-clamp-2 text-sm text-muted-foreground">{story.description}</p>
              </div>
            </Link>
          );
        })}
      </div>
    </main>
  );
};
