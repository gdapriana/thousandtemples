"use client";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import { Book, Bookmark, Map, MessageCircle, Settings2, Trash } from "lucide-react";
import { ConfirmationDialog } from "@/components/confirmation-dialog";
import { useContext } from "react";
import { LoadingContext } from "@/lib/providers/loading";
import { Session } from "next-auth";
import { UserProps } from "@/lib/utils";
import { useRouter } from "next/navigation";

export const Actions = ({
  name,
  readTime,
  session,
  user,
  slug,
  favorite,
  comment,
}: {
  name: string;
  readTime: number | undefined;
  session: Session | null;
  user: UserProps;
  slug: string;
  favorite: { favorited: boolean; count: number };
  comment: number;
}) => {
  const { setLoading } = useContext(LoadingContext);
  const router = useRouter();
  return (
    <main className="flex flex-wrap items-center justify-between gap-2 border-y py-4 md:col-span-2">
      <div className="flex items-center justify-start gap-2 text-sm">
        <Book className="h-4 w-4" />
        {readTime} minutes read
      </div>
      <div className="ms-auto flex items-center justify-center gap-2">
        {session?.user?.email === user.email && (
          <Button size="sm" variant="secondary" className="gap-1" asChild>
            <Link href={`/stories/${slug}/edit`} passHref>
              <Settings2 className="h-4 w-4" />
              <span className="hidden md:inline">Edit</span>
            </Link>
          </Button>
        )}
        {session?.user?.email === user.email && (
          <ConfirmationDialog
            data={{
              title: "Delete Story",
              subtitle: `Are you sure you want to delete ${name}?`,
              button: { icon: Trash, text: "Delete", variant: "secondary" },
              action: () => {
                setLoading(true);
                fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/stories/${slug}`, { method: "DELETE" }).then(() => {
                  router.push("/profile");
                });
              },
            }}
          />
        )}
        <Button size="sm" asChild className="gap-1" variant="secondary">
          <Link href="#comment">
            <MessageCircle className="h-4 w-4" />
            {comment} <span className="hidden md:inline">Comment</span>
          </Link>
        </Button>
        {!session && (
          <Button size="sm" className="gap-1" asChild variant="secondary">
            <Link href="/signin">
              <Bookmark className="h-4 w-4" /> Favorite
            </Link>
          </Button>
        )}
        {session && (
          <ConfirmationDialog
            data={{
              title: favorite.favorited ? "Remove from favorite?" : "Add to favorite?",
              subtitle: favorite.favorited
                ? "Are you sure you want to remove?"
                : "Are you sure you want to add to Favorite?",
              button: {
                icon: Bookmark,
                text: favorite.favorited ? `(${favorite.count}) Unfavorite` : `(${favorite.count}) Favorite`,
                variant: favorite.favorited ? "default" : "secondary",
              },
              action: () => {
                setLoading(true);
                if (favorite.favorited) {
                  fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/stories/${slug}/favorite`, {
                    method: "DELETE",
                    cache: "no-cache",
                  }).then(() => {
                    location.reload();
                  });
                }
                if (!favorite.favorited) {
                  fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/stories/${slug}/favorite`, {
                    method: "POST",
                    cache: "no-cache",
                  }).then(() => {
                    location.reload();
                    setLoading(false);
                  });
                }
              },
            }}
          />
        )}
      </div>
    </main>
  );
};
