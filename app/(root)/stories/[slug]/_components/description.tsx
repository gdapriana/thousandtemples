import Link from "next/link";
import { UserProps } from "@/lib/utils";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { Clock } from "lucide-react";
import moment from "moment";

export const Description = ({
  title,
  description,
  user,
  createdAt,
}: {
  title: string;
  description: string;
  user: UserProps;
  createdAt: string;
}) => {
  return (
    <main className="flex flex-col items-start justify-center gap-2">
      <h1 className="text-xl font-bold md:text-2xl">{title}</h1>
      <div className="mb-4 flex flex-wrap items-center justify-start gap-1 text-sm text-muted-foreground">
        <Clock className="h-4 w-4" />
        <p>{moment(createdAt).fromNow()}</p>
      </div>
      <p className="text-sm text-muted-foreground">{description}</p>
      <Link
        href={`/profile/${user.email}`}
        className="mt-4 flex w-auto items-center justify-center gap-2 rounded-full bg-secondary py-2 pl-2 pr-4"
      >
        <Avatar>
          <AvatarFallback>{user.name?.charAt(0)}</AvatarFallback>
          <AvatarImage src={user.image || ""} />
        </Avatar>
        <div className="flex flex-col items-start justify-center">
          <h1 className="text-sm font-bold">{user.name}</h1>
          <p className="text-xs text-muted-foreground">{user.email}</p>
        </div>
      </Link>
    </main>
  );
};
