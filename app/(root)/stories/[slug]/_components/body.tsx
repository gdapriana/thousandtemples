export const Body = ({ body }: { body: string | undefined }) => {
  return <main dangerouslySetInnerHTML={{ __html: body || "" }} className="prose prose-sm"></main>;
};
