import { Hero } from "@/app/(root)/stories/_components/hero/hero";
import { AllItems } from "@/app/(root)/stories/_components/all-items/all-items";

export default async function StoriesPage() {
  
  const favStories = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/stories?saved=1&take=3`, { cache: "no-cache" })
    .then((res) => res.json())
    .then((data) => data.stories);

  return (
    <main className="flex flex-col gap-20 justify-start items-stretch">
      <Hero favStories={favStories} />
      <AllItems />
    </main>
  )
}