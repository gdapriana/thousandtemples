import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import prisma from "@/lib/db";
import { getServerSession } from "next-auth";
import { NextRequest, NextResponse } from "next/server";

export async function POST(req: NextRequest, { params }: { params: { slug: string } }) {
  const session = await getServerSession(authOptions);
  if (!session) return NextResponse.json({ message: "unauthorized" }, { status: 401 });

  const destination = await prisma.destination.findUnique({
    where: {
      slug: params.slug,
    },
  });

  if (!destination) return NextResponse.json({ message: "destination not found" }, { status: 404 });

  const save = await prisma.usersFavoriteDestinations.create({
    data: {
      destinationSlug: params.slug,
      userEmail: session.user?.email as string,
    },
    select: {
      destination: true,
    },
  });

  return NextResponse.json({ save }, { status: 200 });
}

export async function DELETE(req: NextRequest, { params }: { params: { slug: string } }) {
  const session = await getServerSession(authOptions);
  if (!session) return NextResponse.json({ message: "unauthorized" }, { status: 401 });

  const destination = await prisma.destination.findUnique({
    where: {
      slug: params.slug,
    },
  });

  if (!destination) return NextResponse.json({ message: "destination not found" }, { status: 404 });

  const issave = await prisma.usersFavoriteDestinations.findUnique({
    where: {
      userEmail_destinationSlug: {
        userEmail: session.user?.email as string,
        destinationSlug: params.slug,
      },
    },
  });

  if (!issave) return NextResponse.json({ message: "not saved yet" }, { status: 404 });

  const unsave = await prisma.usersFavoriteDestinations.delete({
    where: {
      userEmail_destinationSlug: {
        userEmail: session.user?.email as string,
        destinationSlug: params.slug,
      },
    },
    select: {
      destination: true,
    },
  });

  return NextResponse.json({ unsave }, { status: 200 });
}
