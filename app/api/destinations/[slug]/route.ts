import prisma from "@/lib/db";
import { NextRequest, NextResponse } from "next/server";

export async function GET(req: NextRequest, { params }: { params: { slug: string } }) {
  const slug = params.slug;
  const destination = await prisma.destination.findUnique({
    where: {
      slug,
    },
    include: {
      _count: true,
      category: true,
      commentedByUsers: {
        include: {
          user: {
            select: {
              name: true,
              id: true,
              image: true,
              email: true,
            },
          },
        },
      },
      district: true,
      favoritedByUsers: true,
      images: {
        include: { destination: true },
      },
      ratedByUsers: true,
    },
  });
  if (!destination) {
    return NextResponse.json({ message: "destination not found" }, { status: 404 });
  }
  return NextResponse.json({ destination }, { status: 200 });
}
