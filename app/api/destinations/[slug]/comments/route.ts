import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import prisma from "@/lib/db";
import { getServerSession } from "next-auth";
import { NextRequest, NextResponse } from "next/server";

export async function POST(req: NextRequest, { params }: { params: { slug: string } }) {
  const session = await getServerSession(authOptions);
  const { body } = await req.json();
  if (!session) return NextResponse.json({ message: "unauthorized" }, { status: 401 });
  const destination = await prisma.destination.findUnique({
    where: {
      slug: params.slug,
    },
  });
  if (!destination) return NextResponse.json({ message: "destination not found" }, { status: 404 });

  const comment = await prisma.usersCommentDestinations.create({
    data: {
      body,
      userEmail: session.user?.email as string,
      destinationSlug: params.slug,
    },
    select: {
      destination: true,
    },
  });

  return NextResponse.json({ comment }, { status: 200 });
}
