import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import prisma from "@/lib/db";
import { getServerSession } from "next-auth";
import { NextRequest, NextResponse } from "next/server";

export async function DELETE(req: NextRequest, { params }: { params: { slug: string; id: string } }) {
  const session = await getServerSession(authOptions);
  if (!session) return NextResponse.json({ message: "unauthorized" }, { status: 401 });
  const destination = await prisma.destination.findUnique({
    where: {
      slug: params.slug,
    },
  });
  if (!destination) return NextResponse.json({ message: "destination not found" }, { status: 404 });

  const commented = await prisma.usersCommentDestinations.findUnique({
    where: {
      id: params.id,
    },
  });

  if (!commented) return NextResponse.json({ message: "comments not found" }, { status: 404 });

  const uncomment = await prisma.usersCommentDestinations.delete({
    where: {
      id: params.id,
    },
    select: {
      destination: true,
    },
  });

  return NextResponse.json({ uncomment }, { status: 200 });
}
