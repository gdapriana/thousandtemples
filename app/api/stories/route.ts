import prisma from '@/lib/db';
import { getServerSession } from 'next-auth';
import { NextRequest, NextResponse } from 'next/server';
import { authOptions } from '../auth/[...nextauth]/route';
import slugify from 'slugify';
import { z } from 'zod';

export async function GET(req: NextRequest) {
  const nameParams =
    (req.nextUrl.searchParams.get('name') as string) || undefined;
  const itemTaken =
    (Number(req.nextUrl.searchParams.get('take')) as number) || undefined;
  const mostSavedParams =
    (Number(req.nextUrl.searchParams.get('saved')) as number) || 0;
  const mostCommentParams =
    (Number(req.nextUrl.searchParams.get('commented')) as number) || 0;
  const newest = (Number(req.nextUrl.searchParams.get('newest')) as number) || undefined;

  console.log({mostSavedParams})
  console.log({mostCommentParams})

  const stories = await prisma.story.findMany({
    where: {
      AND: [{ name: { contains: nameParams, mode: 'insensitive' } }],
    },
    take: itemTaken,
    select: {
      _count: true,
      cover: true,
      id: true,
      slug: true,
      name: true,
      description: true,
      user: true,
    },
    orderBy: [
      {favoritedByUsers: { _count: mostSavedParams === 1 ? 'desc' : 'asc' }},
      {commentedByUsers: { _count: mostCommentParams === 1 ? 'desc' : 'asc' }},
      {createdAt: newest === 1 ? 'desc' : 'asc'}
    ],
  });
  return NextResponse.json({ stories }, { status: 200 });
}

export async function POST(req: NextRequest) {
  const session = await getServerSession(authOptions);
  if (!session)
    return NextResponse.json({ message: 'unauthorized' }, { status: 401 });

  const schema = z.object({
    name: z.string().min(5).max(100),
    description: z.string().min(10).max(400),
    body: z.string().min(10),
    readTime: z.string().optional(),
    cover: z.string().url(),
  });

  const response = schema.safeParse(await req.json());

  if (!response.success) {
    const { errors } = response.error;
    return NextResponse.json(
      { message: 'invalid request', errors },
      { status: 400 },
    );
  }

  const { name, description, body, readTime, cover } = response.data;
  const slug = `${slugify(name, { replacement: '-', lower: true })}-${new Date().getTime()}`;
  const convertReadTime = Number(readTime);

  const story = await prisma.story.create({
    data: {
      slug,
      name,
      description,
      body,
      readtime: convertReadTime,
      cover,
      userEmail: session.user?.email as string,
    },
    select: {
      name: true,
    },
  });

  return NextResponse.json({ story }, { status: 200 });
}
