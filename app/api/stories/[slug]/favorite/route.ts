import { authOptions } from '@/app/api/auth/[...nextauth]/route';
import prisma from '@/lib/db';
import { getServerSession } from 'next-auth';
import { NextRequest, NextResponse } from 'next/server';

export async function POST(
  req: NextRequest,
  { params }: { params: { slug: string } },
) {
  const session = await getServerSession(authOptions);
  if (!session)
    return NextResponse.json({ message: 'unauthorized' }, { status: 401 });

  const story = await prisma.story.findUnique({
    where: {
      slug: params.slug,
    },
  });

  if (!story)
    return NextResponse.json({ message: 'story not found' }, { status: 404 });

  const save = await prisma.usersFavoriteStories.create({
    data: {
      storySlug: params.slug,
      userEmail: session.user?.email as string,
    },
    select: {
      story: true,
    },
  });

  return NextResponse.json({ save }, { status: 200 });
}

export async function DELETE(
  req: NextRequest,
  { params }: { params: { slug: string } },
) {
  const session = await getServerSession(authOptions);
  if (!session)
    return NextResponse.json({ message: 'unauthorized' }, { status: 401 });

  const story = await prisma.story.findUnique({
    where: {
      slug: params.slug,
    },
  });

  if (!story)
    return NextResponse.json(
      { message: 'destination not found' },
      { status: 404 },
    );

  const issave = await prisma.usersFavoriteStories.findUnique({
    where: {
      userEmail_storySlug: {
        userEmail: session.user?.email as string,
        storySlug: params.slug,
      },
    },
  });

  if (!issave)
    return NextResponse.json({ message: 'not saved yet' }, { status: 404 });

  const unsave = await prisma.usersFavoriteStories.delete({
    where: {
      userEmail_storySlug: {
        userEmail: session.user?.email as string,
        storySlug: params.slug,
      },
    },
    select: {
      story: true,
    },
  });

  return NextResponse.json({ unsave }, { status: 200 });
}
