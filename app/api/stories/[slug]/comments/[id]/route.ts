import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import prisma from "@/lib/db";
import { getServerSession } from "next-auth";
import { NextRequest, NextResponse } from "next/server";

export async function DELETE(req: NextRequest, { params }: { params: { slug: string; id: string } }) {
  const session = await getServerSession(authOptions);
  if (!session) return NextResponse.json({ message: "unauthorized" }, { status: 401 });

  const story = await prisma.story.findUnique({
    where: {
      slug: params.slug,
    },
  });

  if (!story) return NextResponse.json({ message: "story not found" }, { status: 404 });

  const commented = await prisma.usersCommentStories.findUnique({
    where: {
      id: params.id,
    },
  });

  if (!commented) return NextResponse.json({ message: "comments not found" }, { status: 404 });

  const uncomment = await prisma.usersCommentStories.delete({
    where: {
      id: params.id,
    },
    select: {
      story: true,
    },
  });

  return NextResponse.json({ uncomment }, { status: 200 });
}
