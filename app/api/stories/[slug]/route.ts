import prisma from '@/lib/db';
import { getServerSession } from 'next-auth';
import { NextRequest, NextResponse } from 'next/server';
import { authOptions } from '../../auth/[...nextauth]/route';

export async function GET(
  req: NextRequest,
  { params }: { params: { slug: string } },
) {
  const story = await prisma.story.findUnique({
    where: {
      slug: params.slug,
    },
    include: {
      _count: true,
      user: true,
      commentedByUsers: {
        include: { user: true },
      },
      favoritedByUsers: true,
      images: {
        include: { destination: true },
      },
    },
  });
  if (!story) {
    return NextResponse.json({ message: 'story not found' }, { status: 404 });
  }
  return NextResponse.json({ story }, { status: 200 });
}

export async function DELETE(
  req: NextRequest,
  { params }: { params: { slug: string } },
) {
  const session = await getServerSession(authOptions);
  if (!session)
    return NextResponse.json({ message: 'unatuhorized' }, { status: 403 });

  const story = await prisma.story.findUnique({
    where: {
      slug: params.slug,
    },
  });

  if (!story)
    return NextResponse.json({ message: 'story not found' }, { status: 404 });

  const deletedStory = await prisma.story.delete({
    where: {
      slug: params.slug,
    },
    select: {
      name: true,
    },
  });

  return NextResponse.json({ deletedStory }, { status: 200 });
}