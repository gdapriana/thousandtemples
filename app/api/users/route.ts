import prisma from '@/lib/db';
import { NextRequest, NextResponse } from 'next/server';

export async function GET(req: NextRequest) {
  const nameParams =
    (req.nextUrl.searchParams.get('name') as string) || undefined;
  const users = await prisma.user.findMany({
    where: {
      AND: [{ name: { contains: nameParams, mode: 'insensitive' } }],
    },
  });
  return NextResponse.json({ users }, { status: 200 });
}
