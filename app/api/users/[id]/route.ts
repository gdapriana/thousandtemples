import prisma from "@/lib/db";
import { NextRequest, NextResponse } from "next/server";

export async function GET(req: NextRequest, { params }: { params: { id: string } }) {
  const user = await prisma.user.findUnique({
    where: {
      email: params.id,
    },
    include: {
      _count: true,
      commentedCultures: true,
      commentedDestinations: true,
      commentedStories: true,
      favoritedStories: { select: { story: true } },
      stories: {
        include: {
          _count: true,
        },
      },
      favoritedCultures: {
        select: {
          culture: { include: { category: true, district: true } },
        },
      },
      favoritedDestinations: {
        select: {
          userEmail: true,
          destination: {
            include: { district: true, category: true },
          },
        },
      },
    },
  });

  if (!user) return NextResponse.json({ message: "user not found" }, { status: 404 });

  return NextResponse.json({ user }, { status: 200 });
}
