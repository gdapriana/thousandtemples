import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import prisma from "@/lib/db";
import { getServerSession } from "next-auth";
import { NextRequest, NextResponse } from "next/server";

export async function POST(req: NextRequest, { params }: { params: { slug: string } }) {
  const session = await getServerSession(authOptions);
  if (!session) return NextResponse.json({ message: "unauthorized" }, { status: 401 });

  const culture = await prisma.culture.findUnique({
    where: {
      slug: params.slug,
    },
  });

  if (!culture) return NextResponse.json({ message: "story not found" }, { status: 404 });

  const save = await prisma.usersFavoriteCultures.create({
    data: {
      cultureSlug: params.slug,
      userEmail: session.user?.email as string,
    },
    select: {
      culture: true,
    },
  });

  return NextResponse.json({ save }, { status: 200 });
}

export async function DELETE(req: NextRequest, { params }: { params: { slug: string } }) {
  const session = await getServerSession(authOptions);
  if (!session) return NextResponse.json({ message: "unauthorized" }, { status: 401 });

  const culture = await prisma.culture.findUnique({
    where: {
      slug: params.slug,
    },
  });

  if (!culture) return NextResponse.json({ message: "destination not found" }, { status: 404 });

  const issave = await prisma.usersFavoriteCultures.findUnique({
    where: {
      userEmail_cultureSlug: {
        userEmail: session.user?.email as string,
        cultureSlug: params.slug,
      },
    },
  });

  if (!issave) return NextResponse.json({ message: "not saved yet" }, { status: 404 });

  const unsave = await prisma.usersFavoriteCultures.delete({
    where: {
      userEmail_cultureSlug: {
        userEmail: session.user?.email as string,
        cultureSlug: params.slug,
      },
    },
    select: {
      culture: true,
    },
  });

  return NextResponse.json({ unsave }, { status: 200 });
}
