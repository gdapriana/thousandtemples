import prisma from "@/lib/db";
import { NextRequest, NextResponse } from "next/server";

export async function GET(req: NextRequest, { params }: { params: { slug: string } }) {
  const culture = await prisma.culture.findUnique({
    where: {
      slug: params.slug,
    },

    include: {
      _count: true,
      category: true,
      district: true,
      commentedByUsers: {
        include: { user: true },
      },
      favoritedByUsers: true,
      images: {
        include: { destination: true },
      },
    },
  });
  if (!culture) {
    return NextResponse.json({ message: "culture not found" }, { status: 404 });
  }
  return NextResponse.json({ culture }, { status: 200 });
}
