import prisma from "@/lib/db";
import { NextRequest, NextResponse } from "next/server";

export async function GET(req: NextRequest) {
  const nameParams = (req.nextUrl.searchParams.get("name") as string) || undefined;
  const addressParams = (req.nextUrl.searchParams.get("address") as string) || undefined;
  const districtParams = (req.nextUrl.searchParams.get("district") as string) || undefined;
  const categoryParams = (req.nextUrl.searchParams.get("category") as string) || undefined;
  const itemTaken = (Number(req.nextUrl.searchParams.get("take")) as number) || undefined;
  const mostSavedParams = (Number(req.nextUrl.searchParams.get("saved")) as number) || 0;
  const mostCommentParams = (Number(req.nextUrl.searchParams.get("commented")) as number) || 0;

  const cultures = await prisma.culture.findMany({
    where: {
      AND: [
        { name: { contains: nameParams, mode: "insensitive" } },
        { address: { contains: addressParams, mode: "insensitive" } },
        { districtSlug: { contains: districtParams, mode: "insensitive" } },
        { categorySlug: { contains: categoryParams, mode: "insensitive" } },
      ],
    },
    take: itemTaken,
    select: {
      _count: true,
      category: true,
      cover: true,
      id: true,
      slug: true,
      name: true,
      description: true,
      address: true,
      district: true,
    },
    orderBy: [
      { favoritedByUsers: { _count: mostSavedParams === 1 ? "desc" : "asc" } },
      {
        commentedByUsers: { _count: mostCommentParams === 1 ? "desc" : "asc" },
      },
    ],
  });
  return NextResponse.json({ cultures }, { status: 200 });
}
