import Image from "next/image";

export const ImageCover = ({ image }: { image: string }) => {
  return (
    <Image
      src={image || ""}
      alt="image"
      width={1000}
      height={1000}
      className="hidden h-full w-1/2 object-cover md:flex"
    />
  );
};
