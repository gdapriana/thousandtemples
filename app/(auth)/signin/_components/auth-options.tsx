"use client";
import { metadata } from "@/lib/metadata";
import { Brand } from "@/app/(root)/_components/header/_components/brand";
import Image from "next/image";
import { signIn } from "next-auth/react";

export const AuthOptions = () => {
  return (
    <main className="flex w-full flex-col items-center justify-center gap-4 p-12 md:w-1/2">
      <Brand link={"/logo.png"} text={metadata.name} />
      <p className="text-center text-sm text-muted-foreground">{metadata.description}</p>
      <div
        onClick={() => signIn("google")}
        className="mt-4 flex cursor-pointer items-center justify-center rounded-full bg-secondary pr-4 font-bold"
      >
        <Image src={"/google.webp"} alt="logo" width={1000} height={1000} className="h-12 w-12" />
        Sign in with Google
      </div>
    </main>
  );
};
