import { ImageCover } from "@/app/(auth)/signin/_components/image";
import { AuthOptions } from "@/app/(auth)/signin/_components/auth-options";

export default function SiginPage() {
  return (
    <main className="absolute left-0 top-0 z-10 flex h-screen w-full items-center justify-center overflow-hidden bg-primary-foreground">
      <AuthOptions />
      <ImageCover image={"/signin-image.jpg"} />
    </main>
  );
}
