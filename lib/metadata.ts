import { LucideIcon, Sunset, TreePalm, UserPen } from "lucide-react";

export interface Metadata {
  name: string;
  description: string;
  logo: string;
  highlight: string;
  routes: {
    name: string;
    path: string;
  }[];
  suports: {
    name: string;
    path: string;
  }[];
  navigations: {
    name: string;
    description: string;
    icon: LucideIcon;
    route: string;
  }[];
}

export const metadata: Metadata = {
  name: "thousandtemples",
  description:
    "ThousandTemples” is a comprehensive project dedicated to exploring and showcasing the diverse destinations, traditions, and cultures of Bali. It provides insights into the island’s iconic temples, scenic landscapes, and vibrant cultural practices, offering a deep dive into Bali’s spiritual heritage, rituals, arts, and local customs.",
  logo: "/logo.png",
  highlight: 'Uncover hidden gems and immerse yourself in the rich culture of Bali',
  routes: [
    { name: "Home", path: "/" },
    { name: "Stories", path: "/stories" },
    { name: "Destinations", path: "/destinations" },
    { name: "Cultures", path: "/cultures" },
  ],
  suports: [
    { name: "Contact", path: "/contact" },
    { name: "Privacy & Policy", path: "/privacypolicy" },
    { name: "Help Center", path: "/helpcenter" },
    { name: "Term of Service", path: "/termofservice" },
  ],
  navigations: [
    {
      name: "Stories",
      description:
        "Dive into captivating stories that reveal Bali’s rich culture, personal travel experiences, and local legends, showcasing the island's unique charm.",
      icon: UserPen,
      route: "/stories",
    },
    {
      name: "Destinations",
      description:
        "Explore Bali’s breathtaking landscapes, from pristine beaches to lush rice terraces and ancient temples, each offering a unique adventure waiting to be discovered.",
      icon: TreePalm,
      route: "/destinations",
    },
    {
      name: "Cultures",
      description:
        "Immerse yourself in the diverse cultures of Bali, where vibrant traditions, unique customs, and warm hospitality bring the island's communities to life.",
      icon: Sunset,
      route: "/Cultures",
    },
  ],
};
