import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/ui/alert-dialog";
import { LucideIcon } from "lucide-react";
import { Button } from "@/components/ui/button";
import { cn } from "@/lib/utils";

export const ConfirmationDialog = ({
  data,
}: {
  data: {
    button?: { text?: string; icon?: LucideIcon; variant?: "outline" | "secondary" | "default"; className?: string };
    title: string;
    subtitle: string;
    action: () => void;
  };
}) => {
  return (
    <AlertDialog>
      <AlertDialogTrigger asChild>
        <Button variant={data.button?.variant || "default"} className={cn(`gap-1`, data.button?.className)} size="sm">
          {data.button?.icon && <data.button.icon className="h-4 w-4" />}
          {data.button?.text && data.button.text}
        </Button>
      </AlertDialogTrigger>
      <AlertDialogContent>
        <AlertDialogTitle>{data.title}</AlertDialogTitle>
        <AlertDialogDescription>{data.subtitle}</AlertDialogDescription>
        <AlertDialogFooter>
          <AlertDialogCancel>Cancel</AlertDialogCancel>
          <AlertDialogAction onClick={data.action}>Yes</AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};
