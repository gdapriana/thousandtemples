import { LucideIcon } from "lucide-react";
import { Button } from "@/components/ui/button";
import { Dialog, DialogContent, DialogFooter, DialogTitle, DialogTrigger } from "@/components/ui/dialog";
import { cn } from "@/lib/utils";
import { Textarea } from "@/components/ui/textarea";
import { Dispatch, SetStateAction } from "react";

export const CommentDialog = ({
  data,
}: {
  data: {
    button?: { text?: string; icon?: LucideIcon; variant?: "outline" | "secondary" | "default"; className?: string };
    title: string;
    subtitle: string;
    action: () => void;
    type: "destinations" | "cultures" | "stories";
    slug: string;
    setLoading: Dispatch<SetStateAction<boolean>>;
  };
}) => {
  const submitHandle = (e: any) => {
    e.preventDefault();
    const body = e.target.body.value;
    fetch(`${process.env.NEXT_PUBLIC_SERVER}/api/${data.type}/${data.slug}/comments`, {
      method: "POST",
      body: JSON.stringify({ body }),
    }).then(() => {
      location.reload();
    });
  };
  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button variant={data.button?.variant || "default"} className={cn("gap-1", data.button?.className)} size="sm">
          {data.button?.icon && <data.button.icon className="h-4 w-4" />}
          {data.button?.text && data.button.text}
        </Button>
      </DialogTrigger>
      <DialogContent>
        <DialogTitle>{data.title}</DialogTitle>
        <form action="" onSubmit={submitHandle} className="flex w-full flex-col items-stretch justify-start gap-4">
          <Textarea name="body" minLength={4} />
          <DialogFooter>
            <Button type="submit" size="sm">
              Send
            </Button>
          </DialogFooter>
        </form>
      </DialogContent>
    </Dialog>
  );
};
